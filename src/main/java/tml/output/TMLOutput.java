package tml.output;

import java.io.IOError;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.util.function.Consumer;

import tml.tree.GenericNode;

/**
 * Static utility class for outputing TML Strings and files which adhere to the
 * TML specification for trees and files.
 * @author J-Dierberger
 * @version 1.0
 */
public final class TMLOutput {

    /**
     * dnu.
     */
    private TMLOutput() { }

    /**
     * Write a mark-up tree forest to the given writer. The nodes provided are
     * treated as heads of the trees of the forest. This forest is written with
     * the given indentation per level and appropriate whitespace. No comments
     * are written. This method functions equivalently to writing the
     * indentation per level specs and then making repeated calls to
     * {@link tml.output.TMLOutput#writeMarkUpTree(GenericNode, int, Writer)},
     * ignoring null heads.
     * @param ipl The indentation per level of the trees being written.
     * @param writer The Writer to write the given forest to.
     * @param heads The heads nodes of the trees in this forest.
     * @throws IOException If an IOException occurs while writing.
     * @throws IllegalArgumentException If writer or heads are null.
     * @see tml.output.TMLOutput#writeMarkUpTree(GenericNode, int, Writer)
     */
    public static void writeMarkUpForest(int ipl, Writer writer,
            GenericNode...heads) throws IOException {
        if (writer == null) {
            throw new IllegalArgumentException("Writer cannot be null");
        }
        if (heads == null) {
            throw new IllegalArgumentException("Heads cannot be null.");
        }
        writer.write(":ipl = ");
        writer.write(Integer.toString(ipl));
        writer.write("\n\n");
        for (GenericNode head : heads) {
            if (heads == null) {
                continue;
            }
            writeMarkUpTree(head, ipl, writer);
            writer.write("\n");
        }
    }

    /**
     * Print a mark-up tree forest to the given PrintStream. The nodes provided
     * are treated as heads of the trees of the forest. This forest is printed
     * with the given indentation per level and appropriate whitespace. No
     * comments are written. This method functions equivalently to printing the
     * indentation per level specs and then making repeated calls to
 * {@link tml.output.TMLOutput#printMarkUpTree(GenericNode, int, PrintStream)},
     * ignoring null heads.
     * @param ipl The indentation per level of the trees being written.
     * @param printer The PrintStream to write the given forest to.
     * @param heads The heads nodes of the trees in this forest.
     * @throws IllegalArgumentException If printer or heads are null.
     * @see tml.output.TMLOutput#printMarkUpTree(GenericNode, int, PrintStream)
     */
    public static void printMarkUpForest(int ipl, PrintStream printer,
            GenericNode...heads) {
        if (printer == null) {
            throw new IllegalArgumentException("PrintStream cannot be null");
        }
        if (heads == null) {
            throw new IllegalArgumentException("Heads cannot be null.");
        }
        printer.print(":ipl = ");
        printer.print(Integer.toString(ipl));
        printer.print("\n\n");
        for (GenericNode head : heads) {
            if (heads == null) {
                continue;
            }
            printMarkUpTree(head, ipl, printer);
            printer.println();
        }
    }

    /**
     * Create a String representation of a mark-up tree forest. The nodes
     * provided are treated as heads of the trees of the forest. This String is
     * created with the given indentation per level and appropriate whitespace.
     * No comments are written. This method functions equivalently to
     * concatenating the indentation per level specs and then repeatedly
     * concatenating calls to
     * {@link tml.output.TMLOutput#stringMarkUpTree(GenericNode, int)},
     * ignoring null heads.
     * @param ipl The indentation per level of the trees being written
     * @param heads The heads nodes of the trees in this forest.
     * @return A String representation of the given forest.
     * @throws IllegalArgumentException If heads is null.
     * @see tml.output.TMLOutput#stringMarkUpTree(GenericNode, int)
     */
    public static String stringMarkUpForest(int ipl, GenericNode...heads) {
        if (heads == null) {
            throw new IllegalArgumentException("Heads cannot be null.");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(":ipl = ").append(ipl).append("\n\n");
        for (GenericNode head : heads) {
            if (heads == null) {
                continue;
            }
            sb.append(stringMarkUpTree(head, ipl)).append("\n");
        }
        return sb.toString();
    }

    /**
     * Write a mark-up tree. The GenericNode provided is regarded as the head
     * of this tree. The tree will be written with the appropriate indentation
     * given, however, the indentation per level is not recorded in the writer.
     * No comments are written with this method. The tree given is written
     * recursively, writing it from left to right level by level, recursing if
     * a node has children. Null children are ignored.<br>
     * <br>
     * The tree generated is guaranteed to be re-parseable by
     * {@link tml.parser.Parser} and the data and tree structure is guaranteed
     * to be identical when it is re-parsed but the data is <i>not</i>
     * guaranteed to be in the same <i>format</i> as the data which the tree
     * was originally parsed from. One such example could be that all nodes
     * generated by this method will opt to use decimal (base 10) for numeric
     * types as opposed to hexadecimal (base 16), even if the original node was
     * parsed from hexadecimal. It is up to the implementation of this method
     * if formatting is preserved.
     * @param head The head of the tree.
     * @param ipl the indentation per level of the tree.
     * @param writer The Writer to append to.
     * @throws IOException If an IOException occurs while writing.
     * @throws IllegalArgumentException If the writer or head is null, or if
     * ipl is {@literal <=} 0.
     */
    public static void writeMarkUpTree(GenericNode head, int ipl,
            Writer writer) throws IOException {
        if (writer == null) {
            throw new IllegalArgumentException("Writer cannot be null");
        }
        try {
            markUpPrintHelper(head, ipl, 0, s -> {
                try {
                    writer.append(s);
                } catch (IOException e) {
                    throw new IOError(e);
                }
            });
        } catch (IOError ioe) {
            throw new IOException(ioe);
        }
        writer.flush();
    }

    /**
     * Print a mark-up tree. The GenericNode provided is regarded as the head
     * of this tree. The tree will be printed with the appropriate indentation
     * given, however, the indentation per level is not recorded in the
     * printer. No comments are printed with this method. The tree given is
     * printed recursively, printing it from left to right level by level,
     * recursing if a node has children. Null children are ignored.<br>
     * <br>
     * The tree generated is guaranteed to be re-parseable by
     * {@link tml.parser.Parser} and the data and tree structure is guaranteed
     * to be identical when it is re-parsed but the data is <i>not</i>
     * guaranteed to be in the same <i>format</i> as the data which the tree
     * was originally parsed from. One such example could be that all nodes
     * generated by this method will opt to use decimal (base 10) for numeric
     * types as opposed to hexadecimal (base 16), even if the original node was
     * parsed from hexadecimal. It is up to the implementation of this method
     * if formatting is preserved.
     * @param head The head of the tree.
     * @param ipl the indentation per level of the tree.
     * @param printer The PrintStream to write to.
     * @throws IllegalArgumentException If the printer or head is null, or if
     * ipl is {@literal <=} 0.
     */
    public static void printMarkUpTree(GenericNode head, int ipl,
            PrintStream printer) {
        if (printer == null) {
            throw new IllegalArgumentException("PrintStream cannot be null");
        }
        markUpPrintHelper(head, ipl, 0, printer::print);
    }

    /**
     * Create a String mark-up tree. The GenericNode provided is regarded as the
     * head of this tree. The tree String will be created with the appropriate
     * indentation given, however, the indentation per level is not recorded
     * in the String. No comments are generated with this method. The tree
     * given is concatenated to the String recursively, generating it from left
     * to right level by level, recursing if a node has children. Null children
     * are ignored.<br>
     * <br>
     * The tree generated is guaranteed to be re-parseable by
     * {@link tml.parser.Parser} and the data and tree structure is guaranteed
     * to be identical when it is re-parsed but the data is <i>not</i>
     * guaranteed to be in the same <i>format</i> as the data which the tree
     * was originally parsed from. One such example could be that all nodes
     * generated by this method will opt to use decimal (base 10) for numeric
     * types as opposed to hexadecimal (base 16), even if the original node was
     * parsed from hexadecimal. It is up to the implementation of this method
     * if formatting is preserved.
     * @param head The head of the tree.
     * @param ipl the indentation per level of the tree.
     * @return A String representation of the given tree.
     * @throws IllegalArgumentException If the printer or head is null, or if
     * ipl is {@literal <=} 0.
     */
    public static String stringMarkUpTree(GenericNode head, int ipl) {
        StringBuilder sb = new StringBuilder();
        markUpPrintHelper(head, ipl, 0, sb::append);
        return sb.toString();
    }

    /**
     * Helper for tree print.
     * @param curr Current node.
     * @param indent Indent per level.
     * @param level Level.
     * @param append The method to append. Shouldn't include newlines.
     */
    private static void markUpPrintHelper(GenericNode curr, int indent,
            int level, Consumer<CharSequence> append) {
        if (curr == null) {
            throw new IllegalArgumentException("Node cannot be null");
        }
        if (indent <= 0) {
            throw new IllegalArgumentException("Indentation-per-level"
                    + " must be > 0");
        }
        StringBuilder indentString = new StringBuilder();
        for (int i = 0; i < indent; i++) {
            indentString.append(" ");
        }
        for (int i = 0; i < level; i++) {
            append.accept(indentString);
        }
        append.accept(curr.toString());
        append.accept("\n");
        for (GenericNode node : curr.getChildren()) {
            if (node != null) {
                markUpPrintHelper(node, indent, level + 1, append);
            }
        }
    }

}

package tml.parser;

/**
 * Indicates a syntactical issue which prevents a tree from being parsed.
 * @author J-Dierberger
 * @version 1.0
 */
public class BadSyntaxException extends RuntimeException {

    /**
     * UID.
     */
    private static final long serialVersionUID = -3378264183982333876L;

    /**
     * Create a BadSyntaxException with the given message.
     * @param msg The message of this exception.
     */
    public BadSyntaxException(String msg) {
        super(msg);
    }

    /**
     * Create a BadSyntaxException with the given cause.
     * @param cause The cause of this exception.
     */
    public BadSyntaxException(Throwable cause) {
        super(cause);
    }

}

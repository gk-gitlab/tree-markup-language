package tml.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import tml.tree.GenericNode;

/**
 * Static utility class for parsing TML Strings and files which adhere to the
 * TML specification for trees and files.
 * @author J-Dierberger
 * @version 1.0
 */
public final class Parser {

    /**
     * dnu.
     */
    private Parser() { }

    /**
     * String form of a regex for a comma which is not inside quotes.
     */
    private static final String COMMANOTINQUOTES
        = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";

    /**
     * String form of a regex for a node.
     */
    private static final String NODEREGEX
        = "<(\\s*,?([^<>]*|\".*\"))*>";

    /**
     * Parse a TML file and get a forest of trees from the file. The heads of
     * each tree in the forest dictated by this file are returned in a List
     * of GenericNodes. This method reads in the configuration options of the
     * file and then delegates to {@link Parser#makeForest(Scanner, int)}.
     * @param file The file to be parsed.
     * @return The forest contained in this file in the same way as makeForest.
     * @throws FileNotFoundException If the file is not found.
     * @throws BadSyntaxException If the TML file contains bad or un-parseable
     * syntax which is in contradiction to the TML language specification.
     * @throws IllegalArgumentException If the indentation per level specified
     * by the file is less than or equal to 0.
     * @see Parser#makeForest(Scanner, int)
     */
    public static List<GenericNode> parseFile(File file)
            throws FileNotFoundException {
        int ipl = 2;
        Scanner sc = new Scanner(file);
        String line = sc.hasNextLine() ? sc.nextLine() : "";
        if (!line.isEmpty() && line.matches("\\s*" + NODEREGEX + "\\s*")) {
            sc.close();
            sc = new Scanner(file);
            return makeForest(sc, ipl);
        }
        if (!line.isEmpty() && !line.matches("\\s*#.*")
                && !line.matches("\\s*:.*")) {
            sc.close();
            throw new BadSyntaxException("Non-comment, non-tree, non-config "
                + "line: " + line);
        }
        if (line.matches("\\s*:.*")) {
            sc.close();
            sc = new Scanner(file);
        }
        boolean go = true;
        while (go && sc.hasNextLine()) {
            line = sc.nextLine().trim();
            if (!line.isEmpty() && line.matches("\\s*" + NODEREGEX + "\\s*")) {
                sc.close();
                throw new BadSyntaxException("Start of tree not separated "
                    + "from configuration");
            } else if (!line.isEmpty() && line.matches("\\s*:.*")) {
                // Configuration option.
                line = line.substring(line.indexOf(":") + 1).trim();
                String[] optionData = line.split("=");
                switch (optionData[0].trim()) {
                case "indentation_per_level": //$FALL-THROUGH$
                case "ipl":
                    try {
                        ipl = Integer.parseInt(optionData[1].trim());
                        if (ipl <= 0) {
                            throw new IllegalArgumentException(
                                    "Indentation required to make trees.");
                        }
                    } catch (NumberFormatException nfe) {
                        throw new IllegalArgumentException(nfe);
                    }
                    break;
                default:
                    break;
                }
            } else if (!line.isEmpty() && !line.matches("\\s*#.*")) {
                // Not empty; not a comment
                sc.close();
                throw new BadSyntaxException("Non-comment, non-configuration "
                    + "line: " + line);
            } else {
                // Empty.
                go = false;
            }
        }
        return makeForest(sc, ipl);
    }

    /**
     * Parse a TML forest String and get a forest of trees from the file. The
     * heads of each tree in the forest dictated by this String are returned in
     * a List of GenericNodes. This function creates a Scanner for the given
     * String and then delegates to {@link Parser#makeForest(Scanner, int)}.
     * @param forest The String containing the forest to parse.
     * @param indentPerLevel The indentation per level of a tree.
     * @return A List of GenericNodes containing the head of each tree in this
     * forest.
     * @see Parser#makeForest(Scanner, int)
     */
    public static List<GenericNode> makeForest(String forest,
            int indentPerLevel) {
        return makeForest(new Scanner(forest), indentPerLevel);
    }

    /**
     * Parse a forest from the given Scanner. Each line of the forest is read
     * and parsed for a node according to the TML standard for forests. The
     * heads of each tree in the forest dictated by this String are returned in
     * a List of GenericNodes.
     * @param sc The Scanner which returns lines from the forest to parse.
     * @param indentPerLevel The indentation per level of a tree in this
     * forest.
     * @return A List of GenericNodes containing the head of each tree in this
     * forest.
     * @throws BadNodeFormatException If a tree's head starts at a non-zero
     * indentation level.
     * @throws BadSyntaxException If the Scanner returns a line not in
     * compliance with the TML language specification.
     */
    public static List<GenericNode> makeForest(Scanner sc,
            int indentPerLevel) {
        List<GenericNode> list = new LinkedList<GenericNode>();
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            // We've found a valid line.
            if (!line.matches("\\s*#.*") && !line.matches("\\s*")
                    && !line.isEmpty()) {
                if (line.matches(NODEREGEX + "\\s*")) {
                    if (line.matches("\\s+" + NODEREGEX + "\\s*")) {
                        throw new BadNodeFormatException("Tree starts at non-"
                            + "zero indentation");
                    }
                    list.add(makeTree(sc, indentPerLevel,
                            getNodeFromLine(line)));
                } else {
                    throw new BadSyntaxException("Non-node, non-comment line: "
                        + line);
                }
            }
        }
        sc.close();
        return list;
    }


    /**
     * Parse a tree from a String containing a tree according to the TML
     * language specification. Returns the head of the tree parsed. This
     * function delegates to
     * {@link Parser#makeTree(Scanner, int, GenericNode)}.
     * @param tree The String which contains the tree to parse.
     * @param indentPerLevel The indentation per level of the tree being
     * parsed.
     * @return The head node for this tree.
     * @see Parser#makeTree(Scanner, int, GenericNode)
     */
    public static GenericNode makeTree(String tree, int indentPerLevel) {
        return makeTree(new Scanner(tree), indentPerLevel, null);
    }

    /**
     * Parse a tree from the given Scanner according to the TML language
     * specification. Returns the head of the tree parsed.
     * @param sc The Scanner which returns the lines to parse.
     * @param indentPerLevel The indentation per level of the tree.
     * @param head The head to start at. This parameter is primarily used for
     * when a line has already been scanned in. If this parameter is null, the
     * Scanner will be read until the head is found.
     * @return The head node for this tree.
     * @throws IllegalArgumentException If an indentPerLevel {@literal <=} 0
     * is given, or if no head is given and the Scanner parses a head at a
     * non-zero indentation level.
     * @throws BadSyntaxException If an indentation level is illegally skipped.
     */
    public static GenericNode makeTree(Scanner sc, int indentPerLevel,
            GenericNode head) {
        if (indentPerLevel <= 0) {
            throw new IllegalArgumentException(
                    "Indentation required to make trees.");
        }
        /* ******** Tree setup ******** */
        //  We first create a Scanner for this tree string.
        String line = null;
        while (head == null && sc.hasNextLine()) {
            line = sc.nextLine();
            head = getNodeFromLine(line);
            if (head != null && line.matches("\\s+.*")) {
                throw new IllegalArgumentException(
                        "Tree must start at indentation 0");
            }
        }
        if (head == null) {
            return null;
        }

        /* ******** Parsing setup ******** */
        // This stack keeps track of the roots of sub-trees.
        Deque<GenericNode> stack = new LinkedList<GenericNode>();
        // Go ahead and add the head.
        stack.push(head);
        // Indicate the current level of indentation
        int stackLevel = 0;

        /* ******** Parsing ******** */
        parsingNodes:
            while (sc.hasNextLine()) {
                line = sc.nextLine();
                // If the line is empty we stop
                if (line.replaceAll("\\s", "").isEmpty()) {
                    break parsingNodes;
                }
                // We need to count the indentation of the current node.
                int level = 0;
                for (int i = 0; i < line.length() && (line.charAt(i) == ' '
                        || line.charAt(i) == '\t'); i++) {
                    if (line.charAt(i) == ' ') {
                        level++;
                    } else if (line.charAt(i) == '\t') {
                        level += 4;
                    }
                }
                // Calculate the actual indentation level.
                level /= indentPerLevel;
                // Get the node on the current line.
                GenericNode node = getNodeFromLine(line);
                if (node == null) { // Indicates a comment-only line
                    continue parsingNodes;
                }

                /* ******** Adding to tree ******** */
                if (level > stackLevel) {
                    // Add ourself to the previous node.
                    // Note we don't pop it: other nodes may want it.
                    stack.peekFirst().addChildren(node);
                    // Push ourself to the stack as the new root.
                    stack.push(node);
                    // Mark the new level.
                    stackLevel = level;
                } else if (level == stackLevel) {
                    // Remove the old head; its on the same level as us.
                    stack.pop();
                    // Now we can add to the old parent.
                    stack.peekFirst().addChildren(node);
                    // If the next line is a child, it needs to reference node.
                    stack.push(node);
                } else {
                    // Pop until we are at the correct level.
                    while (level <= stackLevel) {
                        stack.pop();
                        stackLevel--;
                    }
                    // Now we are at the level of our parent; add ourself.
                    if (stack.peekFirst() == null) {
                        throw new BadSyntaxException("Indentation skipped");
                    }
                    stack.peekFirst().addChildren(node);
                    // Push ourself as the next node on our level.
                    stack.push(node);
                    // We overshot; fix.
                    stackLevel = level;
                }
            }
        return head;
    }

    /**
     * Create a node from a line of a TML file.
     * @param line The line to convert to a node.
     * @return The node parsed from the given line.
     */
    private static GenericNode getNodeFromLine(String line) {
        if (line.matches("\\s*#.*") || line.matches("\\s*")) {
            return null; // Comment only/empty lines are null.
        }
        line = line.trim();
        if (!line.matches(NODEREGEX)) {
            throw new BadNodeFormatException('"' + line + '"'
                    + " must match regex '" + NODEREGEX + "'");
        }
        // Ensure all quotes are closed.
        int quoteCount = 0;
        for (int i = line.indexOf('"'); i > 0
                && i < line.lastIndexOf('>'); i++) {
            quoteCount += line.charAt(i) == '"' ? 1 : 0;
        }
        if (quoteCount % 2 != 0) {
            throw new BadSyntaxException("Unclosed quotes: " + line);
        }
        // Create the node
        String dataAttr = line.substring(
                line.indexOf("<") + 1,
                line.lastIndexOf(">"));
        String[] dataAndAttr = dataAttr.split(COMMANOTINQUOTES);
        Object[] attr = new Object[dataAndAttr.length - 1];
        for (int i = 0; i < attr.length; i++) {
            attr[i] = getData(dataAndAttr[i + 1].trim());
        }
        return new GenericNode(getData(dataAndAttr[0]), attr);
    }

    /**
     * Get the data from the given expression.
     * @param expr The expression to parse the data from.
     * @return The data from the given String, the String if
     * the data cannot be inferred.
     */
    private static Object getData(String expr) {
        if (expr.isEmpty()) {
            return null;
        }
        if (expr.equals("null")) {
            return null;
        }
        try {
            if (expr.matches("(\\+|-|)0x[\\dABCDEFabcdef]+")) {
                return Integer.valueOf(expr.replace("0x", ""), 16);
            } else if (expr.matches("(\\+|-|)0b[01]+")) {
                return Integer.valueOf(expr.replace("0b", ""), 2);
            } else {
                return Integer.valueOf(expr);
            }
        } catch (NumberFormatException nfe) {
        }
        try {
            if (expr.matches("(\\+|-|)0x[\\dABCDEFabcdef]+")) {
                return Long.valueOf(expr.replace("0x", ""), 16);
            } else if (expr.matches("(\\+|-|)0b[01]+")) {
                return Long.valueOf(expr.replace("0b", ""), 2);
            } else {
                return Long.valueOf(expr);
            }
        } catch (NumberFormatException nfe) {
        }
        try {
            if (expr.matches("(\\+|-|)0x[\\dABCDEFabcdef]+")) {
                return new BigInteger(expr.replace("0x", ""), 16);
            } else if (expr.matches("(\\+|-|)0b[01]+")) {
                return new BigInteger(expr.replace("0b", ""), 2);
            } else {
                return new BigInteger(expr);
            }
        } catch (NumberFormatException nfe) {
        }
        try {
            return Double.valueOf(expr);
        } catch (NumberFormatException nfe) {
        }
        // replace quotes to make string literals and escaped quotes with quotes
        expr = expr.replace("\\n", "\n");
        expr = expr.replace("\\t", "\t");
        expr = expr.replace("\\r", "\r");
        expr = expr.replace("\\\"", "\u0000");
        expr = expr.replace("\"", "");
        expr = expr.replace("\u0000", "\"");
        expr = expr.replace("\\\\", "\\");
        return expr;
    }

}

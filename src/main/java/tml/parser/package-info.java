/**
 * This package contains component(s) to parse through a .tml file.
 * @author J-Dierberger
 */
package tml.parser;
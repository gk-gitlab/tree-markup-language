package forestout;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import tml.output.TMLOutput;

//import org.junit.Test;

import tml.parser.Parser;
import tml.tree.GenericNode;

public class ForestOutHardTests {

    @SuppressWarnings("unused")
    private static final int TIMEOUT = 200;

    private static final String TREE1STR
        = "<1>\n"
        + "    <2>\n"
        + "    <3>\n"
        + "        <\"\">\n"
        + "    <>\n";
    private static final GenericNode TREE1 = Parser.makeTree(TREE1STR, 4);

//    @Test(timeout = TIMEOUT)
    public void testForestOut1() throws IOException {
        File file = new File("./src/test/out/hard_out_1.tml");
        if (!file.exists()) {
            file.createNewFile();
        }
        TMLOutput.writeMarkUpForest(3, new FileWriter(file), TREE1);
        assertEquals(Arrays.asList(TREE1), Parser.parseFile(file));
    }

    private static final String TREE2STR
        = "<1>\n"
        + "  <2>\n"
        + "    <3>\n"
        + "      <4>\n"
        + "  <2>\n"
        + "    <3>\n"
        + "      <4>\n"
        + "      <4>\n"
        + "        <5>\n"
        + "  <2>\n"
        + "    <3>\n"
        + "      <4>\n"
        + "    <3>\n"
        + "  <2>\n";
    private static final GenericNode TREE2 = Parser.makeTree(TREE2STR, 2);

//    @Test(timeout = TIMEOUT)
    public void testForestOut2() throws IOException {
        File file = new File("./src/test/out/hard_out_2.tml");
        if (!file.exists()) {
            file.createNewFile();
        }
        TMLOutput.writeMarkUpForest(2, new FileWriter(file), TREE2);
        assertEquals(Arrays.asList(TREE2), Parser.parseFile(file));
    }

    private static final String TREE3STR
        = "<1>\n"
        + "  <2>\n"
        + "  <3>\n"
        + "    <\"\">\n"
        + "  <>\n";
    private static final GenericNode TREE3 = Parser.makeTree(TREE3STR, 2);

//    @Test(timeout = TIMEOUT)
    public void testForestOut3() throws IOException {
        File file = new File("./src/test/out/hard_out_3.tml");
        if (!file.exists()) {
            file.createNewFile();
        }
        TMLOutput.writeMarkUpForest(2, new FileWriter(file), TREE3, TREE2);
        assertEquals(Arrays.asList(TREE3, TREE2), Parser.parseFile(file));
    }

//    @Test(timeout = TIMEOUT)
    public void testForestOut4() throws IOException {
        File file = new File("./src/test/out/hard_out_4.tml");
        if (!file.exists()) {
            file.createNewFile();
        }
        TMLOutput.writeMarkUpForest(2, new FileWriter(file));
        assertEquals(Arrays.asList(), Parser.parseFile(file));
    }

}

package makeforest;

import static org.junit.Assert.fail;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import tml.parser.BadNodeFormatException;
import tml.parser.BadSyntaxException;
import tml.parser.Parser;

public class MakeForestErrorTests {

    private static final int TIMEOUT = 200;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    private static final String TREESTR1
            = "<1>\n"
            + "  <2>\n"
            + "    <3>\n"
            + "      <4>\n"
            + "  <2>\n";

    private static final String TREESTR2
            = "<2>\n"
            + "Garbage in the middle!"
            + "  <2>\n";

    @Test(timeout = TIMEOUT)
    public void testNonCommentLineBefore() {
        thrown.expect(BadSyntaxException.class);
        Parser.makeForest("Comment\n" + TREESTR1, 2);
    }

    @Test(timeout = TIMEOUT)
    public void testNonCommentLineAfter() {
        thrown.expect(BadSyntaxException.class);
        Parser.makeForest(TREESTR1 + "\ngarbage", 2);
    }

    @Test(timeout = TIMEOUT)
    public void testNonCommentLineInTheMiddle() {
        thrown.expect(BadNodeFormatException.class);
        Parser.makeForest(TREESTR2 + "\ngarbage", 2);
    }

    @Test(timeout = TIMEOUT)
    public void testConcatWithoutWhitespace() {
        try {
            Parser.makeForest(TREESTR1 + TREESTR2, 2);
            fail("Expected any exception; got nothing");
        } catch (Exception e) {
        }
    }

    @Test(timeout = TIMEOUT)
    public void testConcatWithoutWhitespaceWithComment() {
        try {
            Parser.makeForest(TREESTR1 + "#Comment\n" + TREESTR2, 2);
            fail("Expected any exception; got nothing");
        } catch (Exception e) {
        }
    }

}

package makeforest;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import tml.parser.Parser;
import tml.tree.GenericNode;

public class MakeForestHardTests {

    private static final int TIMEOUT = 200;

    private static final String TREESTR1
            = "<1>\n"
            + "  <2>\n"
            + "    <3>\n"
            + "      <4>\n"
            + "  <2>\n";

    private static final GenericNode TREE1 = Parser.makeTree(TREESTR1, 2);

    private static final String TREESTR2
            = "<2>\n"
            + "#Comment in the middle!"
            + "  <3, 2>\n";

    private static final GenericNode TREE2 = Parser.makeTree(TREESTR2, 2);

    private static final String TREESTR3
        = "<1>\n"
        + "  <2>\n"
        + "    <3>\n"
        + "  <2>\n";

    private static final GenericNode TREE3 = Parser.makeTree(TREESTR3, 2);

    @Test(timeout = TIMEOUT)
    public void testMakeTree1() {
        List<GenericNode> list = Parser.makeForest("#Comment\n" + TREESTR1, 2);
        assertEquals(Arrays.asList(TREE1), list);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree2() {
        List<GenericNode> list = Parser.makeForest(TREESTR2, 2);
        assertEquals(Arrays.asList(TREE2), list);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree3() {
        List<GenericNode> list = Parser.makeForest(TREESTR3 + "\n#Comment", 2);
        assertEquals(Arrays.asList(TREE3), list);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeForest() {
        List<GenericNode> list = Parser.makeForest(
                "\n#Comment\n"
                + TREESTR3
                + "\n\n#Comment test\n"
                + TREESTR1
                + "\n\n#Comment\n"
                + TREESTR2
                + "\n#Comment", 2);
        assertEquals(Arrays.asList(TREE3, TREE1, TREE2), list);
    }

}

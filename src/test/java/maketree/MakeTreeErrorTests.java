package maketree;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import tml.parser.BadNodeFormatException;
import tml.parser.BadSyntaxException;
import tml.parser.Parser;

public class MakeTreeErrorTests {

    private static final int TIMEOUT = 200;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    private static final String TREE1 = "<";
    private static final String TREE2 = ">";
    private static final String TREE3 = "<<>";
    private static final String TREE4 = "<>>";
    private static final String TREE5 = "<<>>";
    private static final String TREE6 = "<\">";
    private static final String TREE7 = "There's nothing here!";
    private static final String TREE8 = "  <Indentation starts at 0>";

    @Test(timeout = TIMEOUT)
    public void testMakeTree1() {
        thrown.expect(BadNodeFormatException.class);
        Parser.makeTree(TREE1, 2);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree2() {
        thrown.expect(BadNodeFormatException.class);
        Parser.makeTree(TREE2, 2);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree3() {
        thrown.expect(BadNodeFormatException.class);
        Parser.makeTree(TREE3, 2);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree4() {
        thrown.expect(BadNodeFormatException.class);
        Parser.makeTree(TREE4, 2);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree5() {
        thrown.expect(BadNodeFormatException.class);
        Parser.makeTree(TREE5, 2);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree6() {
        thrown.expect(BadSyntaxException.class);
        Parser.makeTree(TREE6, 2);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree7() {
        thrown.expect(BadNodeFormatException.class);
        Parser.makeTree(TREE7, 2);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree8() {
        thrown.expect(IllegalArgumentException.class);
        Parser.makeTree(TREE8, 2);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree9() {
        thrown.expect(IllegalArgumentException.class);
        Parser.makeTree("<>", 0);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTreeA() {
        thrown.expect(IllegalArgumentException.class);
        Parser.makeTree("<>", -7);
    }

}

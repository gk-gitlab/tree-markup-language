package maketree;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tml.parser.Parser;
import tml.tree.GenericNode;

public class MakeTreeHardTests {

    private static final int TIMEOUT = 200;

    private static final String TREE1
        = "<\"#\">\n#Check";

    @Test(timeout = TIMEOUT)
    public void testMakeTree1() {
        GenericNode root = Parser.makeTree(TREE1, 2);
        assertEquals("#", root.getData());
    }

    private static final String TREE2
            = "<1> \n"
            + "  <2>\n"
            + "    <>\n"
            + "  <3>\n"
            + "    <\"<>\">\n"
            + "  <null>\n"
            + "    <12>\n"
            + "      <\"null\">\n"
            + "  <10.0>\n"
            + "    <10000000000>";

    @Test(timeout = TIMEOUT)
    public void testMakeTree2() {
        GenericNode root = Parser.makeTree(TREE2, 2);
        assertEquals(Integer.valueOf(1), root.getData());
        assertEquals(Integer.valueOf(2), root.getChildren()[0].getData());
        assertEquals(Integer.valueOf(3), root.getChildren()[1].getData());
        assertEquals(null, root.getChildren()[2].getData());
        assertEquals(Double.valueOf(10.0), root.getChildren()[3].getData());
        assertEquals(null, root.getChildren()[0].getChildren()[0].getData());
        assertEquals("<>", root.getChildren()[1].getChildren()[0].getData());
        assertEquals(Integer.valueOf(12),
                root.getChildren()[2].getChildren()[0].getData());
        assertEquals(Long.valueOf(10000000000L),
                root.getChildren()[3].getChildren()[0].getData());
        assertEquals("null",
                root.getChildren()[2].getChildren()[0]
                .getChildren()[0].getData());
    }

    private static final String TREE3
            = " # Comment, should be ignored\n"
            + "<\"#\"> \n"
            + "  <\\\"#\\\">\n"
            + " #moar comment #\n"
            + "  <#>\n"
            + "    <\"7\">\n"
            + "      <\\\"10.1\\\">\n"
            + "    # \"another # one \"\n"
            + "    <10.0>\n"
            + "      <11.11>";

    @Test(timeout = TIMEOUT)
    public void testMakeTree3() {
        GenericNode root = Parser.makeTree(TREE3, 2);
        assertEquals("#", root.getData());
        assertEquals("\"#\"", root.getChildren()[0].getData());
        assertEquals("#", root.getChildren()[1].getData());
        assertEquals("7", root.getChildren()[1].getChildren()[0].getData());
        assertEquals(Double.valueOf(10.0), root.getChildren()[1]
                .getChildren()[1].getData());
        assertEquals("\"10.1\"", root.getChildren()[1]
                .getChildren()[0]
                .getChildren()[0]
                .getData());
        assertEquals(Double.valueOf(11.11), root.getChildren()[1]
                .getChildren()[1]
                .getChildren()[0]
                .getData());
    }

}

package maketree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigInteger;

import org.junit.Test;

import tml.parser.Parser;
import tml.tree.GenericNode;

public class MakeTreeNodeTests {

    private static final int TIMEOUT = 200;

    private static final String TREE1 = "<0>";
    private static final String TREE2 = "<null>";
    private static final String TREE3 = "<\"null\">";
    private static final String TREE4 = "<\"<>\">";
    private static final String TREE5 = "<100000000000>";
    private static final String TREE6 = "<10.1>";
    private static final String TREE7 = "<Hello world!>";
    private static final String TREE8 = "<\"Hello world!\">";
    private static final String TREE9 = "<>";
    private static final String TREEA = "<\"\">";
    private static final String TREEB = "<\\\"Hello world!\\\">";
    private static final String TREEC = "<0b11>";
    private static final String TREED = "<0x10>";
    private static final String TREEE = "<\\n>";
    private static final String TREEF = "<0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF>";

    @Test(timeout = TIMEOUT)
    public void testMakeSingleNode() {
        GenericNode head1 = Parser.makeTree(TREE1, 2);
        assertNotNull(head1);
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTree1() {
        GenericNode head = Parser.makeTree(TREE1, 2);
        assertEquals(Integer.class, head.getData().getClass());
        assertEquals(Integer.valueOf(0), head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTree2() {
        GenericNode head = Parser.makeTree(TREE2, 2);
        assertEquals(null, head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTree3() {
        GenericNode head = Parser.makeTree(TREE3, 2);
        assertEquals(String.class, head.getData().getClass());
        assertEquals("null", head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTree4() {
        GenericNode head = Parser.makeTree(TREE4, 2);
        assertEquals(String.class, head.getData().getClass());
        assertEquals("<>", head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTree5() {
        GenericNode head = Parser.makeTree(TREE5, 2);
        assertEquals(Long.class, head.getData().getClass());
        assertEquals(Long.valueOf(100000000000L), head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTree6() {
        GenericNode head = Parser.makeTree(TREE6, 2);
        assertEquals(Double.class, head.getData().getClass());
        assertEquals(Double.valueOf(10.1), head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTree7() {
        GenericNode head = Parser.makeTree(TREE7, 2);
        assertEquals(String.class, head.getData().getClass());
        assertEquals("Hello world!", head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTree8() {
        GenericNode head = Parser.makeTree(TREE8, 2);
        assertEquals(String.class, head.getData().getClass());
        assertEquals("Hello world!", head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTree9() {
        GenericNode head = Parser.makeTree(TREE9, 2);
        assertEquals(null, head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTreeA() {
        GenericNode head = Parser.makeTree(TREEA, 2);
        assertEquals(String.class, head.getData().getClass());
        assertEquals("", head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTreeB() {
        GenericNode head = Parser.makeTree(TREEB, 2);
        assertEquals(String.class, head.getData().getClass());
        assertEquals("\"Hello world!\"", head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTreeC() {
        GenericNode head = Parser.makeTree(TREEC, 2);
        assertEquals(Integer.class, head.getData().getClass());
        assertEquals(Integer.valueOf(3), head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTreeD() {
        GenericNode head = Parser.makeTree(TREED, 2);
        assertEquals(Integer.class, head.getData().getClass());
        assertEquals(Integer.valueOf(16), head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTreeE() {
        GenericNode head = Parser.makeTree(TREEE, 2);
        assertEquals(String.class, head.getData().getClass());
        assertEquals("\n", head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

    @Test(timeout = TIMEOUT)
    public void testNodeTreeF() {
        GenericNode head = Parser.makeTree(TREEF, 2);
        assertEquals(BigInteger.class, head.getData().getClass());
        assertEquals(new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16),
                head.getData());
        for (GenericNode node : head.getChildren()) {
            assertEquals(null, node);
        }
    }

}

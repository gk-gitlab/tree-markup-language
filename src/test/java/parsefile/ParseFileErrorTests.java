package parsefile;

import java.io.FileNotFoundException;
import java.nio.file.Paths;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import tml.parser.BadSyntaxException;
import tml.parser.Parser;

public class ParseFileErrorTests {

    private static final int TIMEOUT = 200;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test(timeout = TIMEOUT)
    public void testParseFile1() throws FileNotFoundException {
        thrown.expect(BadSyntaxException.class);
        Parser.parseFile(
                Paths.get("src", "test", "resources", "error", "error1.tml")
                .toFile());
    }

    @Test(timeout = TIMEOUT)
    public void testParseFile2() throws FileNotFoundException {
        thrown.expect(BadSyntaxException.class);
        Parser.parseFile(
                Paths.get("src", "test", "resources", "error", "error2.tml")
                .toFile());
    }

    @Test(timeout = TIMEOUT)
    public void testParseFile3() throws FileNotFoundException {
        thrown.expect(BadSyntaxException.class);
        Parser.parseFile(
                Paths.get("src", "test", "resources", "error", "error3.tml")
                .toFile());
    }

    @Test(timeout = TIMEOUT)
    public void testParseFile4() throws FileNotFoundException {
        thrown.expect(BadSyntaxException.class);
        Parser.parseFile(
                Paths.get("src", "test", "resources", "error", "error4.tml")
                .toFile());
    }

    @Test(timeout = TIMEOUT)
    public void testParseFile5() throws FileNotFoundException {
        thrown.expect(IllegalArgumentException.class);
        Parser.parseFile(
                Paths.get("src", "test", "resources", "error", "error5.tml")
                .toFile());
    }

    @Test(timeout = TIMEOUT)
    public void testParseFile6() throws FileNotFoundException {
        thrown.expect(IllegalArgumentException.class);
        Parser.parseFile(
                Paths.get("src", "test", "resources", "error", "error6.tml")
                .toFile());
    }

    @Test(timeout = TIMEOUT)
    public void testParseFile7() throws FileNotFoundException {
        thrown.expect(IllegalArgumentException.class);
        Parser.parseFile(
                Paths.get("src", "test", "resources", "error", "error7.tml")
                .toFile());
    }

}

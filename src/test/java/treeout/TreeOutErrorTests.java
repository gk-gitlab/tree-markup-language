package treeout;

import java.io.IOException;

import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import tml.output.TMLOutput;
import tml.tree.GenericNode;

public class TreeOutErrorTests {

    private static final int TIMEOUT = 200;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test(timeout = TIMEOUT)
    public void testTreeOut1() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(CoreMatchers.containsString("null"));
        TMLOutput.stringMarkUpTree(null, 2);
    }

    @Test(timeout = TIMEOUT)
    public void testTreeOut2() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(CoreMatchers
                .anyOf(CoreMatchers.containsString("indentation-per-level"),
                CoreMatchers.containsString("Indentation-per-level")));
        TMLOutput.stringMarkUpTree(new GenericNode(), 0);
    }

    @Test(timeout = TIMEOUT)
    public void testTreeOut3() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(CoreMatchers
                .anyOf(CoreMatchers.containsString("indentation-per-level"),
                CoreMatchers.containsString("Indentation-per-level")));
        TMLOutput.stringMarkUpTree(new GenericNode(), -7);
    }

    @Test(timeout = TIMEOUT)
    public void testTreeOut4() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(CoreMatchers.containsString("null"));
        TMLOutput.printMarkUpTree(new GenericNode(), 2, null);
    }

    @Test(timeout = TIMEOUT)
    public void testTreeOut5() throws IOException {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(CoreMatchers.containsString("null"));
        TMLOutput.writeMarkUpTree(new GenericNode(), 2, null);
    }

}

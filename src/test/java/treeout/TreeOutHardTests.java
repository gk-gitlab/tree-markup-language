package treeout;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tml.output.TMLOutput;
import tml.parser.Parser;
import tml.tree.GenericNode;

public class TreeOutHardTests {

    private static final int TIMEOUT = 200;

    private static final String TREE1STR
        = "<1>\n"
        + "   <2>\n"
        + "   <3>\n"
        + "      <\"\">\n"
        + "   <>\n";
    private static final GenericNode TREE1 = Parser.makeTree(TREE1STR, 3);

    @Test(timeout = TIMEOUT)
    public void testTreeOut1() {
        assertEquals(TREE1, Parser.makeTree(
                TMLOutput.stringMarkUpTree(TREE1, 5), 5));
    }

    private static final String TREE2STR
        = "<1>\n"
        + "  <2>\n"
        + "    <3>\n"
        + "      <4>\n"
        + "  <2>\n"
        + "    <3>\n"
        + "      <4>\n"
        + "      <4>\n"
        + "        <5>\n"
        + "  <2>\n"
        + "    <3>\n"
        + "      <4>\n"
        + "    <3>\n"
        + "  <2>\n";
    private static final GenericNode TREE2 = Parser.makeTree(TREE2STR, 2);

    @Test(timeout = TIMEOUT)
    public void testTreeOut2() {
        assertEquals(TREE2, Parser.makeTree(
                TMLOutput.stringMarkUpTree(TREE2, 2), 2));
    }

}
